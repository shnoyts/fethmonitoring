# coding: utf-8

import datetime
import socket
import struct

from Crypto.Hash import SHA256

from ..common.config import DEFAULT_IP
from ..common.config import DEFAULT_PORT
from ..common.magic_numbers import M_PM25_PM10
from ..common.crypto import CryptoStep
from ..common.network import get_inet
from ..common.setup import Step
from ..common.setup import entry_point_setup


class Listener(Step):
    def __init__(self):
        super().__init__({
            ('network', 'port'): (int, DEFAULT_PORT),
            ('network', 'listen_ip'): (lambda x: x, DEFAULT_IP),
        })

    def setup(self, config):
        listen_ip = config['network']['listen_ip']
        listen_port = config['network']['port']
        inet = get_inet(listen_ip)

        self.sock = sock = socket.socket(
            inet,
            socket.SOCK_DGRAM  # UDP
        )

        sock.bind((listen_ip, listen_port))

    def fetch(self):
        # TODO: check buffers...
        return self.sock.recvfrom(1024)  # buffer size


class CryptoReader(CryptoStep):
    def __init__(self):
        super().__init__(
            'private_decrypt', './private-readings.key',
            'sign_pub_pi', './sign_pub_pi.key'
        )

    def check_integrity(self, data, sender):
        encrypted, signature = data[:512], data[512:]
        digest = SHA256.new(encrypted)

        checksig = self.checksigner.verify(digest, signature)

        if not checksig:
            self.logger.debug('Invalid signature from %s:%s', sender)
            return

        decrypted_msg = self.decipher.decrypt(encrypted, self.error_decrypt)
        if decrypted_msg is self.error_decrypt:
            self.logger.debug("Error decrypting message")
            return None

        return decrypted_msg


class Unpacker(Step):
    def __init__(self):
        super().__init__({})

    def setup(self, config):
        pass

    def process(self, data, senderinfo):
        magic_number = data[:2]
        if magic_number == M_PM25_PM10:
            epoch, pm_25, pm_10 = struct.unpack("!Lff", data[2:])
            date = datetime.datetime.fromtimestamp(epoch)
            return {
                'date': date,
                'pm 2.5': pm_25,
                'pm 10': pm_10,
            }
        else:
            self.logger.debug('unknown packet magic number')


class GnocchiFeeder(Step):
    """TODO"""
    def __init__(self):
        super().__init__({})

    def setup(self, config):
        pass

    def insert(self, data, senderinfo):
        #  FIXME: insert in db
        # asserting this is sds11
        print(
            "received data from {}:{}: {} PM2.5: {: >#04.1f} "
            "PM10: {: >#04.1f}".format(
                senderinfo[0], senderinfo[1], data['date'], data['pm 2.5'],
                data['pm 10']
            )
        )


def receive_forever():
    listener = Listener()
    crypto_reader = CryptoReader()
    unpacker = Unpacker()
    gnocchi = GnocchiFeeder()
    steps = listener, crypto_reader, unpacker, gnocchi

    entry_point_setup('receiver', steps)

    print('loop started')
    while True:
        data, senderinfo = listener.fetch()
        decrypted = crypto_reader.check_integrity(data, senderinfo)
        if not decrypted:
            continue
        structured = unpacker.process(decrypted, senderinfo)
        gnocchi.insert(structured, senderinfo)
