# coding: utf-8

"""
Sensor collection. Here: sds11 only
"""

import datetime
import socket
import serial
import time
import struct

from Crypto.Hash import SHA256

from ..common.config import DEFAULT_IP
from ..common.config import DEFAULT_PORT
from ..common.magic_numbers import M_PM25_PM10
from ..common.network import get_inet
from ..common.setup import Step
from ..common.crypto import CryptoStep
from ..common.setup import entry_point_setup


class SDS11Packer(Step):
    def __init__(self):
        super().__init__({})

    def setup(self, config):
        self.ser = serial.Serial(
            "/dev/ttyUSB0", baudrate=9600, stopbits=1, parity="N", timeout=2
        )
        self.ser.flushInput()
        self.curbyte, self.lastbyte = b"\x00", b"\x00"
        self.logger.debug("now reading from usb")

    def fetch(self):
        self.lastbyte = self.curbyte
        self.curbyte = self.ser.read(size=1)
        # We got a valid packet header
        if self.lastbyte == b"\xAA" and self.curbyte == b"\xC0":
            sentence = self.ser.read(size=8)  # Read 8 more bytes
            readings = struct.unpack('<hhxxcc', sentence)  # Decode the packet
            #  - big endian, 2 shorts for pm2.5 and pm10, 2 reserved bytes,
            #  checksum, message tail

            pm_25 = readings[0]/10.0
            pm_10 = readings[1]/10.0
            now = datetime.datetime.now()

            self.logger.debug(
                "PM 2.5: %s μg/m^3  PM 10: %s μg/m^3",
                pm_25, pm_10,
            )

            now = int(time.time())

            # long, float, float
            msg = struct.pack("!Lff", now, pm_25, pm_10)

            return M_PM25_PM10 + msg


class CryptoWriter(CryptoStep):
    def __init__(self):
        super().__init__(
            'sign_priv_pi', './sign_pub_priv.key',
            'public_crypt', './private-readings.key.pub',
        )

    def crypt(self, msg):
        encrypted_msg = self.cipher.encrypt(msg)  # 512 bytes

        digest = SHA256.new(encrypted_msg)
        signature = self.signer.sign(digest)  # 512 bytes

        #  REMOVEME : check we are failing sig
        #  signature = signature[:41] + b'a' + signature[42:]

        return encrypted_msg + signature  # 1024 bytes


class Sender(Step):
    def __init__(self):
        super().__init__({
            ('network', 'port'): (int, DEFAULT_PORT),
            ('network', 'dest_ip'): (lambda x: x, DEFAULT_IP),
        })

    def setup(self, config):
        self.dest_ip = config['network']['dest_ip']
        self.dest_port = config['network']['port']
        self.inet = get_inet(self.dest_ip)

    def send(self, msg):
        sock = socket.socket(
            self.inet,
            socket.SOCK_DGRAM  # UDP
        )
        sock.sendto(msg, (self.dest_ip, self.dest_port))


def run():
    packer = SDS11Packer()
    crypto_writer = CryptoWriter()
    sender = Sender()

    steps = packer, crypto_writer, sender

    entry_point_setup('sds11', steps)

    while True:
        msg = packer.fetch()
        if msg is None:
            continue
        encrypted_msg = crypto_writer.crypt(msg)
        sender.send(encrypted_msg)
