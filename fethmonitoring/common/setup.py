import os
import sys

from .config import read_conf
from . import log


class Step:
    def __init__(self, defaults):
        self.defaults = defaults
        self.logger = log.setup(self.__class__.__name__)

    def setup(self, config):
        raise NotImplementedError()


def entry_point_setup(conffile_prefix, steps):
    if len(sys.argv) > 1:
        conf_filename = sys.argv[1]
    else:
        conf_filename = os.path.join(
            os.path.expanduser("~"),
            '{}.ini'.format(conffile_prefix)
        )

    defaults = {}
    for step in steps:
        defaults.update(step.defaults)

    config = read_conf(conf_filename, defaults)

    for step in steps:
        step.setup(config)
