import logging


_INTERNAL_STATE = {
    'was_setup': False
}


def _really_setup():
    formatter = logging.Formatter(
        "%(asctime)s [%(name)-10.10s] %(message)s"
    )
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)
    rootLogger.addHandler(consoleHandler)


def setup(*logger_name):
    if not _INTERNAL_STATE['was_setup']:
        _really_setup()
        _INTERNAL_STATE['was_setup'] = True

    return logging.getLogger(*logger_name)
