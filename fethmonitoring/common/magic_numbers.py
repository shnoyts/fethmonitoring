M_PM25_PM10 = b'PM'

MAGIC_NUMBERS = (
    M_PM25_PM10,
)


# runtime validation that magic numbers take 2 bytes
for magic in MAGIC_NUMBERS:
    assert len(magic) == 2
