import ipaddress
import socket


def get_inet(string_ip):
    if ipaddress.ip_address(string_ip).version == 4:
        return socket.AF_INET
    return socket.AF_INET6
