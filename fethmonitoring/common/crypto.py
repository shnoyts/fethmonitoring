import os

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from Crypto.Signature import PKCS1_v1_5 as pkcs_sign
from Crypto import Random

from .setup import Step


class CryptoStep(Step):
    def __init__(
            self,
            private_confkey, private_default,
            public_confkey, public_default
    ):
        super().__init__({
            ('crypto', private_confkey):
                (lambda x: x, private_default),
            ('crypto', public_confkey):
                (lambda x: x, public_default),
        })
        self.error_decrypt = object()
        self.private_confkey = private_confkey
        self.public_confkey = public_confkey

    def setup(self, config):
        private_key_file = config['crypto'][self.private_confkey]
        self._load_private_key(private_key_file)
        public_key_file = config['crypto'][self.public_confkey]
        self._load_public_key(public_key_file)

    def _load_private_key(self, private_key_file):
        if not os.path.exists(private_key_file):
            self.make_rsa(private_key_file)

        self.logger.debug('loading private key: %s', private_key_file)
        with open(private_key_file, 'r', encoding='ascii') as priv_fd:
            private_key_txt = priv_fd.read()
        private_key = RSA.importKey(private_key_txt)
        self.decipher = PKCS1_v1_5.PKCS115_Cipher(private_key)
        self.signer = pkcs_sign.new(private_key)
        self.logger.debug('loaded private key')

    def _load_public_key(self, public_key_file):
        self.logger.debug('loading public key: %s', public_key_file)
        public_sign_txt = open(
            public_key_file, 'r', encoding='ascii'
        ).read()
        public_key = RSA.importKey(public_sign_txt)
        self.checksigner = pkcs_sign.new(public_key)
        self.cipher = PKCS1_v1_5.PKCS115_Cipher(public_key)
        self.logger.debug('loaded public key')

    def make_rsa(self, filename):
        self.logger.critical("generating key for you")
        random_generator = Random.new().read
        key = RSA.generate(4096, random_generator)

        self.logger.critical("saving private key in %s", filename)
        privexport = key.exportKey('PEM')
        with open(filename, 'wb') as privfd:
            privfd.write(privexport)

        self.logger.critical("saving public key in %s", filename + '.pub')
        publicexport = key.publickey().exportKey('PEM')
        with open(filename + '.pub', 'wb') as pubfd:
            pubfd.write(publicexport)

