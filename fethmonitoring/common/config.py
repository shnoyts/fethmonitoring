from collections import defaultdict
import configparser

from . import log


DEFAULT_PORT = 5005
DEFAULT_IP = '::1'


def read_conf(filename, confkeys_convertersdefaults):
    """
    Reads configuration from ini file and defaults settings.

    :param str filename: ini file to be read
    :param dict confkeys_convertersdefaults: dict of following structure:
        * keys are a duple of 2 strings (section, key) in ini file
        * values are a duple of callable (will convert raw string value) and
          default vavlue
    """
    logger = log.setup('config')
    config = configparser.ConfigParser()
    has_read = config.read(filename)

    if has_read:
        logger.debug("Read config file: %s", filename)
    else:
        logger.debug("Could not read config file: %s", filename)

    result = defaultdict(lambda: {})
    _unset = object()

    for (
            (section, key), (converter, default)
    ) in confkeys_convertersdefaults.items():
        if section in config:
            raw_value = config.get(section, key, fallback=_unset)
        else:
            raw_value = _unset

        if raw_value is _unset:
            logger.debug('%s/%s: using default: %s', section, key, default)
            value = default
        else:
            value = converter(raw_value)

        result[section][key] = value

    return result
