project fethmonitoring
-----------------------

Will rename this project if gets serious.

Project consists of 2 kind of exe:

sensor attached part
---------------------

running on a linux somewhere::

    fethmonitoring-sds11

db attached part
-----------------

Running on a linux somewhere, close to a gnocchi db::

    fethmonitoring-centralize

Getting started
----------------


Requirements
..............

* pycrypto or pycryptodome
* sensor part only: pyserial

2 hurdles
...........

* we rely on asymetric crypto, but happily enough, the exe will generate the required keys.
* install gnocchi.
